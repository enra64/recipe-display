from PyQt5.QtWidgets import QLabel

from src.popo.recipe import Recipe


class HighlightableLabel:
    def __init__(self, controlled_label: QLabel):
        self.controlled_label = controlled_label
        self._recipe = None  # type: Recipe

    def load_recipe(self, recipe: Recipe):
        self._recipe = recipe
        self._update_label()

    def forward(self):
        if self._recipe:
            self._recipe.forward()
            self._update_label()

    def backward(self):
        if self._recipe:
            self._recipe.backward()
            self._update_label()

    def _update_label(self):
        result = []

        for i, sentence in enumerate(self._recipe.sentences):
            if i == self._recipe.highlighted_sentence_index:
                result.append("<b>{}</b>".format(self._recipe.sentences[i]))
            else:
                result.append(self._recipe.sentences[i])

        self.controlled_label.setText("".join(result))

        self.controlled_label.layout()
