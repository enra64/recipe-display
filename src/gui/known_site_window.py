from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QLabel, QStackedWidget, QWidget, QVBoxLayout

from src.gui.auto_adjust_label import AutoAdjustLabel
from src.gui.highlightable_label import HighlightableLabel
from src.popo.recipe import Recipe


class KnownSiteWindow(QWidget):
    def __init__(self, parent: QWidget = None):
        super().__init__(parent)
        central_layout = QVBoxLayout()
        self.setLayout(central_layout)
        central_layout.setContentsMargins(50, 50, 50, 50)

        self._add_recipe_label(central_layout)

        self._ingredientInstructionSwitcher = QStackedWidget()
        central_layout.addWidget(self._ingredientInstructionSwitcher)

        self._add_ingredients_label(self._ingredientInstructionSwitcher)
        self._add_instructions_label(self._ingredientInstructionSwitcher)

        self._recipe_to_load = None
        self.timer = QTimer()
        self.timer.timeout.connect(self._load_recipe)
        self.timer.start(1000)

    def _add_ingredients_label(self, stack: QStackedWidget):
        self._ingredients_label = AutoAdjustLabel()
        stack.addWidget(self._ingredients_label)

        self._ingredients_label.setWordWrap(True)
        self._ingredients_label.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        self._ingredients_label.setText("")

    def _add_instructions_label(self, stack: QStackedWidget):
        instructions_label = AutoAdjustLabel()
        stack.addWidget(instructions_label)

        instructions_label.setWordWrap(True)
        instructions_label.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        instructions_label.setText("")

        self._instructions_highlightable_label = HighlightableLabel(instructions_label)

    def _add_recipe_label(self, main_layout: QVBoxLayout):
        self._recipe_label = QLabel()
        main_layout.addWidget(self._recipe_label)

        larger_font = self._recipe_label.font()
        larger_font.setPointSize(40)
        self._recipe_label.setWordWrap(True)
        self._recipe_label.setFont(larger_font)
        self._recipe_label.setText("Lade Rezept")

    def stop(self):
        self.timer.stop()

    def forward_instructions(self):
        self._instructions_highlightable_label.forward()

    def backward_instructions(self):
        self._instructions_highlightable_label.backward()

    def show_ingredients(self):
        self._ingredientInstructionSwitcher.setCurrentWidget(self._ingredients_label)

    def show_instructions(self):
        self._ingredientInstructionSwitcher.setCurrentWidget(self._instructions_highlightable_label.controlled_label)

    def load_recipe(self, value: Recipe):
        self._recipe_to_load = value

    def _load_recipe(self):
        if self._recipe_to_load is not None:
            self._instructions_highlightable_label.load_recipe(self._recipe_to_load)
            self._ingredients_label.setText(self._recipe_to_load.get_ingredient_names())
            self._recipe_label.setText(self._recipe_to_load.title)
            self._recipe_to_load = None
