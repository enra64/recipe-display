from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtWidgets import QWidget, QVBoxLayout, qApp

from src.util.config import config
from src.webserver.BlockingNetworkAccessManager import BlockingNetworkAccessManager


class UnknownSiteWindow(QWidget):
    def __init__(self, parent: QWidget = None):
        super().__init__(parent)
        central_layout = QVBoxLayout()
        central_layout.setContentsMargins(9, 0, 0, 0)
        self.setLayout(central_layout)

        self._recipe_to_load = None

        self.browser = QWebView(self)
        central_layout.addWidget(self.browser)

        if config.gui.enable_adblock:
            blocking_nam = BlockingNetworkAccessManager(self)
            self.browser.page().setNetworkAccessManager(blocking_nam)

        self.timer = QTimer()
        self.timer.moveToThread(qApp.thread())
        self.timer.timeout.connect(self._load_recipe)
        self.timer.start(1000)

    def stop(self):
        self.timer.stop()

    def forward_instructions(self):
        self.browser.setZoomFactor(self.browser.zoomFactor() * 0.9)

    def backward_instructions(self):
        self.browser.setZoomFactor(self.browser.zoomFactor() * 1.1)

    def show_ingredients(self):
        self.browser.page().mainFrame().scroll(0, -self.browser.height())

    def show_instructions(self):
        self.browser.page().mainFrame().scroll(0, self.browser.height())

    def load_recipe(self, value: str):
        self._recipe_to_load = value

    def _load_recipe(self):
        if self._recipe_to_load is not None:
            self.browser.load(QUrl(self._recipe_to_load))
            self._recipe_to_load = None
