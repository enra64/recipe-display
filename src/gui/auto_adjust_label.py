import logging

from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import QResizeEvent, QFontMetrics
from PyQt5.QtWidgets import QLabel, QWidget


class AutoAdjustLabel(QLabel):
    """
    https://stackoverflow.com/a/42690033/5922516
    """

    def __init__(self, parent: QWidget = None):
        super(AutoAdjustLabel, self).__init__(parent)
        self.logger = logging.getLogger("autoLabel")
        self.margin = 50

    def document_rect(self) -> QRect:
        rect = self.contentsRect()
        rect.adjust(self.margin, self.margin, -self.margin, -self.margin)
        return rect

    def layout(self):
        text = self.text()

        if text == "":
            return

        rect_lbl = self.document_rect()
        font = self.font()
        size = font.pointSize()
        font_metrics = QFontMetrics(font)
        rect = font_metrics.boundingRect(rect_lbl, Qt.TextWordWrap, text)

        linebreak_count = text.count("<br>")

        if rect.height() > rect_lbl.height() or rect.width() > rect_lbl.width():
            step = -1
        else:
            step = 1

        while True:
            font.setPointSize(size + step)
            font_metrics = QFontMetrics(font)
            rect = font_metrics.boundingRect(rect_lbl, Qt.TextWordWrap, text)
            rect.adjust(0, 0, 0, linebreak_count * font_metrics.height())

            if size <= 1:
                print("font cannot be made smaller!")
                break

            label_height = rect_lbl.height() + self.margin * 2
            label_width = rect_lbl.width() + self.margin * 2

            if step < 0:
                size += step
                if rect.height() < label_height and rect.width() < label_width:
                    break
            else:
                if rect.height() > label_height or rect.width() > label_width:
                    break
                size += step

        self.logger.debug("going with {}pt".format(size))
        font.setPointSize(size)
        self.setFont(font)

    def resizeEvent(self, resize_event: QResizeEvent):
        super().resizeEvent(resize_event)
        self.layout()
