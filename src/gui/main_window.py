from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QSizePolicy, QStackedWidget

from src.gui.known_site_window import KnownSiteWindow
from src.gui.unknown_site_window import UnknownSiteWindow
from src.popo.recipe import Recipe
from src.util.config import config


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self._current_window_implementation = None  # type: MainWindow or None
        self._knownSiteWindow = KnownSiteWindow(self)
        self._unknownSiteWindow = UnknownSiteWindow(self)
        self._wait_label = self._create_wait_label()
        self.stack = self._setup_stack(self._wait_label, self._knownSiteWindow, self._unknownSiteWindow)

        self.setCentralWidget(self.stack)
        self.stack.setCurrentWidget(self._wait_label)
        self.show_as_configured()

    def load_recipe(self, recipe_type: str, value: Recipe or str):
        if recipe_type == "chefkoch":
            self._current_window_implementation = self._knownSiteWindow
        else:
            self._current_window_implementation = self._unknownSiteWindow

        self.stack.setCurrentWidget(self._current_window_implementation)
        self._current_window_implementation.load_recipe(value)

    def _setup_stack(self, wait_label, known_site_widget, unkown_site_widget) -> QStackedWidget:
        stack = QStackedWidget()
        self.setCentralWidget(stack)
        stack.addWidget(wait_label)
        stack.addWidget(known_site_widget)
        stack.addWidget(unkown_site_widget)
        return stack

    def _create_wait_label(self) -> QLabel:
        wait_label = QLabel()
        wait_label.setText("Warte auf Rezept")
        larger_font = wait_label.font()
        larger_font.setPointSize(40)
        wait_label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        wait_label.setAlignment(Qt.AlignCenter)
        wait_label.setFont(larger_font)
        return wait_label

    def show_as_configured(self):
        if config.gui.fullscreen:
            self.showFullScreen()
        else:
            self.setFixedSize(800, 1280)
            self.show()

    def stop(self):
        if self._current_window_implementation is not None:
            self._current_window_implementation.stop()

    def forward_instructions(self):
        if self._current_window_implementation is not None:
            self._current_window_implementation.forward_instructions()

    def backward_instructions(self):
        if self._current_window_implementation is not None:
            self._current_window_implementation.backward_instructions()

    def show_ingredients(self):
        if self._current_window_implementation is not None:
            self._current_window_implementation.show_ingredients()

    def show_instructions(self):
        if self._current_window_implementation is not None:
            self._current_window_implementation.show_instructions()
