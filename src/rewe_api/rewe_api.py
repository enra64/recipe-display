import requests
from bs4 import BeautifulSoup, NavigableString

from src.popo.recipe import Recipe

__headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'
}


def load_recipe(url: str) -> Recipe:
    page = requests.get(url, headers=__headers)
    soup = BeautifulSoup(page.content)
    # filter(soup.find_all(class_="recipe-step"))
    # instructions[0].select("div p")
    # " ".join([i.get_text() for i in soup.select(".recipe-step div p")])
    recipe_dict = {}
    recipe_dict["instructions"] = " ".join([i.get_text() for i in soup.select(".recipe-step div p")])
    ingredients = [i for i in soup.select(".ingredient-list li")]

    ingredient_group = {
        "header": "Zutaten",
        "ingredients": []
    }
    for ingredient in ingredients:
        name = "".join([t for t in ingredient.find("div").contents if type(t) == NavigableString]).strip()

        unit = ""
        unit_html = ingredient.find(class_="unit")
        if unit_html is not None:
            unit = unit_html.get_text().strip()

        amount = ""
        amount_html = ingredient.find(class_="amount-text")
        if amount_html is not None:
            amount = amount_html.get_text().strip()

        ingredient_group["ingredients"].append({
            "name": name,
            "unit": unit,
            "amount": amount,
            "usageInfo": "",
        })

    recipe_dict["ingredientGroups"] = [ingredient_group]
    recipe_dict["title"] = soup.find(class_="headline").get_text().strip()
    recipe_dict["subtitle"] = ""

    return Recipe(recipe_dict)
