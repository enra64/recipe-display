#!/usr/bin/python
import logging
from http.server import BaseHTTPRequestHandler, HTTPServer
from os import sep, getcwd
from threading import Thread
from urllib.parse import unquote_plus

from src.util.config import config
from src.util.load_recipe_signal import LoadRecipeSignaller

BASE_PATH = "/webserver/content"


class ChefkochHandler(BaseHTTPRequestHandler):
    load_recipe_signaller = None  # type: LoadRecipeSignaller or None
    loaded_link = None  # type: str
    logger = logging.getLogger("ChefkochHandler")
    logger.setLevel(logging.INFO)

    def log_message(self, format_string, *args):
        self.logger.debug(format_string, *args)

    # Handler for the GET requests
    def do_GET(self):
        requested_path = self.path
        if not requested_path.startswith(BASE_PATH):
            requested_path = BASE_PATH + requested_path

        if requested_path == BASE_PATH + "/":
            requested_path = BASE_PATH + sep + "index.html"

        if "handle_link" in requested_path and self.loaded_link != requested_path:
            url = requested_path.split("?")[1].replace("link=", "")
            url = unquote_plus(url)

            if self.load_recipe_signaller is not None:
                self.logger.info("Loading link {}".format(url))
                self.load_recipe_signaller.load_recipe.emit(url)
            else:
                self.logger.error("Link callback is none?!?")

            self.loaded_link = requested_path
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            with open(getcwd() + sep + "src" + sep + BASE_PATH + sep + "success.html", mode='rb') as file:
                self.wfile.write(file.read())
            return

        try:
            # Check the file extension required and
            # set the right mime type

            mimetype = ""

            if requested_path.endswith(".html"):
                mimetype = 'text/html'
            if requested_path.endswith(".jpg"):
                mimetype = 'image/jpg'
            if requested_path.endswith(".svg"):
                mimetype = 'image/svg+xml'
            if requested_path.endswith(".ico"):
                mimetype = 'image/x-icon'
            if requested_path.endswith(".gif"):
                mimetype = 'image/gif'
            if requested_path.endswith(".js"):
                mimetype = 'application/javascript'
            if requested_path.endswith(".css"):
                mimetype = 'text/css'
            if requested_path.endswith("?v=4.7.0"):
                requested_path = requested_path.replace("?v=4.7.0", "")

            # Open the static file requested and send it
            self.send_response(200)
            self.send_header('Content-type', mimetype)
            self.end_headers()

            with open(getcwd() + sep + "src" + sep + requested_path, mode='rb') as file:
                self.wfile.write(file.read())

            return

        except IOError:
            self.send_error(404, 'File Not Found: %s' % self.path)


class RecipeReceivingServer(HTTPServer):
    # noinspection PyMethodOverriding
    def serve_forever(self, load_recipe_signaller: LoadRecipeSignaller, poll_interval=0.5):
        self.RequestHandlerClass.load_recipe_signaller = load_recipe_signaller
        HTTPServer.serve_forever(self, poll_interval)


def start_server(load_recipe_signaller: LoadRecipeSignaller):
    HTTPServer.allow_reuse_address = True
    server = RecipeReceivingServer(('', config.receive_server.port), ChefkochHandler)

    def start():
        try:
            server.serve_forever(load_recipe_signaller)
        except KeyboardInterrupt:
            server.shutdown()
            server.socket.close()

    process = Thread(target=start)
    process.start()

    def shutdown():
        server.shutdown()
        server.socket.close()
        process.join()

    return shutdown
