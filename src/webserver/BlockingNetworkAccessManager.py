from PyQt5.QtCore import QUrl, QObject, QIODevice
from PyQt5.QtNetwork import QNetworkAccessManager, QNetworkRequest
from adblockparser import AdblockRules

from src.util.config import config


class BlockingNetworkAccessManager(QNetworkAccessManager):
    ad_block_filter = None  # type: AdblockRules or None

    def __init__(self, parent: QObject = None):
        super().__init__(parent)
        if BlockingNetworkAccessManager.ad_block_filter is None:
            self._load_rules()

    @staticmethod
    def _load_rules():
        rules = []

        with open("assets/easylist.txt", "r") as f:
            rules.extend(f.read().split("\n"))

        with open("assets/obtrusive.txt", "r") as f:
            rules.extend(f.read().split("\n"))

        with open("assets/fanboy-annoyance.txt", "r") as f:
            rules.extend(f.read().split("\n"))

        BlockingNetworkAccessManager.ad_block_filter = AdblockRules(rules, use_re2=config.receive_server.use_re2)

    def createRequest(self, op, request: QNetworkRequest, device: QIODevice = None):
        url = request.url().toString()
        should_filter = BlockingNetworkAccessManager.ad_block_filter.should_block(url)

        if should_filter:
            print("skipping " + url)
            return super().createRequest(QNetworkAccessManager.GetOperation, QNetworkRequest(QUrl()))
        else:
            return super().createRequest(op, request, device)
