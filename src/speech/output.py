class SpeechOutput:
    def stop(self):
        raise NotImplementedError

    def say(self, string: str):
        raise NotImplementedError()
