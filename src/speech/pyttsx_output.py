import logging
from threading import Thread

import pyttsx3

from src.speech.output import SpeechOutput


class PyttsxSpeechOutput(SpeechOutput):
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._engine = pyttsx3.init(debug=True)
        self._engine.setProperty("voice", "german")
        self._engine.setProperty("rate", 150)
        self._thread = Thread(target=self._engine.startLoop)
        self._thread.start()

    def stop(self):
        try:
            self._engine.stop()
            self._engine.endLoop()
        except RuntimeError:
            pass
        self._thread.join()

    def say(self, string: str):
        if string:
            self._logger.info("Saying {}".format(string))
            self._engine.say(string)
