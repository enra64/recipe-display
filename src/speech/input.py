#!/usr/bin/env python3

# NOTE: this example requires PyAudio because it uses the Microphone class
import logging
from threading import Thread, Event
from typing import Callable

import speech_recognition
# try:
from snowboy import snowboydecoder

from src.util.config import config


# except ImportError:
#    from . import snowboydecoder


class SpeechInput:
    def __init__(self, recognition_callback: Callable[[str], None]):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._recognition_callback = recognition_callback

        # start listening in the background
        self._hotword_detector = snowboydecoder.HotwordDetector(
            config.speech_recognition.hotwords.model,
            sensitivity=config.speech_recognition.hotwords.sensitivity,
            apply_frontend=True
        )
        self._thread = Thread(target=self._run)
        self._thread.start()
        self._stop_flag = Event()

    def stop(self):
        self._stop_flag.set()
        self._thread.join()

    def _should_stop(self):
        if self._stop_flag.is_set():
            self._hotword_detector.terminate()
            return True
        return False

    def _run(self):
        self._hotword_detector.start(
            detected_callback=lambda: self._logger.info("hotword detected; recording"),
            audio_recorder_callback=self._on_audio_input,
            interrupt_check=self._should_stop
        )

    def _on_audio_input(self, audio_file_name: str):
        recognizer = speech_recognition.Recognizer()
        with speech_recognition.AudioFile(audio_file_name) as source:
            audio = recognizer.record(source)

        try:
            # for testing purposes, we're just using the default API key
            # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
            # instead of `r.recognize_google(audio)`
            if config.speech_recognition.tool == "google":
                recognition_result = recognizer.recognize_google(audio, language="de-DE").lower()  # type: str
            elif config.speech_recognition.tool == "sphinx":
                recognition_result = recognizer.recognize_sphinx(audio).lower()  # type: str
            else:
                self._logger.error("unknown speech_recognition.tool {}".format(config.speech_recognition.tool))
                return

            self._logger.info("Speech output is {}".format(recognition_result))
            self._recognition_callback(recognition_result)

        except speech_recognition.UnknownValueError:
            self._logger.warning("Could not understand audio")
        except speech_recognition.RequestError as e:
            self._logger.error("Could not request results from Recognition service; {0}".format(e))
