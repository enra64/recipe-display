import logging
from typing import Callable

from src.popo.ingredient import Ingredient
from src.popo.recipe import Recipe


class ArtificialIntelligence:
    def __init__(self, speech_output_callback: Callable[[str], None]):
        self._trigger_groups = {
            "amount": ["wieviel", "wie viel", "how much"],
            "paragraph": ["absatz", "\n\n", "abschnitt", "paragraph"],
            "sentence": ["sentence", "satz"]
        }
        self.recipe = None  # type: Recipe or None
        self._logger = logging.getLogger(self.__class__.__name__)
        self._speech_output_callback = speech_output_callback

    def on_audio_input(self, recognition_result: str):
        for trigger_group_name, triggers in self._trigger_groups.items():
            for trigger in triggers:
                if recognition_result.startswith(trigger):
                    rest = recognition_result[len(trigger):]
                    self.on_speech_recognition(trigger_group_name, rest)

    def on_speech_recognition(self, trigger_group_name: str, rest: str):
        self._logger.info("Speech recognized '{}'".format(rest))
        if self.recipe is not None:
            if trigger_group_name == "amount":
                for ingredient in self.recipe.get_ingredients():
                    if rest.split(" ")[-1] == ingredient.name.lower():
                        self.on_ingredient_recognition(ingredient)
            elif trigger_group_name == "paragraph":
                self._speech_output_callback(self.recipe.get_current_paragraph())
            elif trigger_group_name == "sentence":
                self._speech_output_callback(self.recipe.get_currently_highlighted_sentence())

    def on_ingredient_recognition(self, ingredient: Ingredient):
        answer = "{} {} {}".format(ingredient.amount, ingredient.get_unit_name(), ingredient.name)
        self._speech_output_callback(answer)
