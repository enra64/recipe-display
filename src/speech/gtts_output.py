import logging
import os

from gtts import gTTS

from src.speech.output import SpeechOutput


class GttsSpeechOutput(SpeechOutput):
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)

    def stop(self):
        pass

    def say(self, string: str):
        if string:
            self._logger.info("Saying {}".format(string))
            tts = gTTS(text=string, lang="de")
            tts.save("/tmp/tts.mp3")
            os.system("mpg123 /tmp/tts.mp3")
