from src.chefkoch_api.chefkoch import ChefkochApi
from src.popo.recipe import Recipe


def get_recipe(recipe_id: int) -> Recipe:
    api = ChefkochApi()
    return Recipe(api.get_recipe(recipe_id))
