import logging
from typing import Callable

from src.gpio.buttons import Buttons


class MockButtons(Buttons):
    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.instructions_light_on = False
        self.ingredients_light_on = False
        self.power_off_callback = self._button_signaler.power_off.emit  # type: Callable
        self.ingredients_callback = self._button_signaler.ingredients.emit  # type: Callable
        self.instructions_callback = self._button_signaler.instructions.emit  # type: Callable
        self.instructions_forward_callback = self._button_signaler.forward.emit  # type: Callable
        self.instructions_backward_callback = self._button_signaler.backward.emit  # type: Callable

    def set_power_off_callback(self, callback: Callable):
        self._button_signaler.power_off.connect(callback)

    def set_ingredients_callback(self, callback: Callable):
        self._button_signaler.ingredients.connect(callback)

    def set_instructions_callback(self, callback: Callable):
        self._button_signaler.instructions.connect(callback)

    def _log_light_states(self):
        self.logger.info("Instructions light on is {}; Ingredients light on is {}".format(self.instructions_light_on,
                                                                                          self.ingredients_light_on))

    def set_instructions_light(self, light_on: bool):
        self.instructions_light_on = light_on
        self._log_light_states()

    def set_ingredients_light(self, light_on: bool):
        self.ingredients_light_on = light_on
        self._log_light_states()

    def set_instructions_forward_callback(self, callback: Callable):
        self._button_signaler.forward.connect(callback)

    def set_instructions_backward_callback(self, callback: Callable):
        self._button_signaler.backward.connect(callback)
