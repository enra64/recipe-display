from typing import Callable

from gpiozero import Button

from src.gpio.pins import Pin


class RotaryEncoder:
    def __init__(self, clock_pin: Pin, data_pin: Pin):
        self.backward_callback = None  # type: Callable[[], None]
        self.forward_callback = None  # type: Callable[[], None]
        self._clock_button = Button(clock_pin.value, pull_up=True)
        self._data_button = Button(data_pin.value, pull_up=True)
        self._clock_button.when_activated = self._rotary_interrupt
        self._data_button.when_activated = self._rotary_interrupt
        self._last_data_value = False
        self._last_clock_value = False

    def _rotary_interrupt(self, interrupt_device):
        clock_value = self._clock_button.is_active
        data_value = self._data_button.is_active

        # now check if state has changed; if not, bouncing caused the interrupt
        if self._last_clock_value == clock_value and self._last_data_value == data_value:
            return

        # remember new state for next bouncing check
        self._last_clock_value = clock_value
        self._last_data_value = data_value

        if clock_value and data_value:  # Both one active? Yes -> end of sequence
            if interrupt_device == self._data_button:
                if self.forward_callback:
                    self.forward_callback()
            else:
                if self.backward_callback:
                    self.backward_callback()
