from typing import Callable

from PyQt5.QtCore import pyqtSignal, QObject


class ButtonSignaler(QObject):
    power_off = pyqtSignal()
    ingredients = pyqtSignal()
    instructions = pyqtSignal()
    forward = pyqtSignal()
    backward = pyqtSignal()


class Buttons:
    def __init__(self):
        self._button_signaler = ButtonSignaler()

    def set_power_off_callback(self, callback: Callable):
        pass

    def set_ingredients_callback(self, callback: Callable):
        pass

    def set_instructions_callback(self, callback: Callable):
        pass

    def set_instructions_light(self, light_on: bool):
        pass

    def set_ingredients_light(self, light_on: bool):
        pass

    def set_instructions_forward_callback(self, callback: Callable):
        pass

    def set_instructions_backward_callback(self, callback: Callable):
        pass
