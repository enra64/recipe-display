from typing import Callable

from gpiozero import Button, LED

from src.gpio.buttons import Buttons
from src.gpio.pins import Pin
# https://www.raspberrypi.org/forums/viewtopic.php?t=198815
from src.gpio.rotary_encoder import RotaryEncoder


class RpiButtons(Buttons):
    def __init__(self):
        super().__init__()
        self._ingredientsButton = Button(Pin.INGREDIENTS_BUTTON.value, pull_up=True)
        self._instructionsButton = Button(Pin.INSTRUCTIONS_BUTTON.value, pull_up=True)

        self._ingredientsLight = LED(Pin.INGREDIENTS_LIGHT.value)
        self._instructionsLight = LED(Pin.INSTRUCTIONS_LIGHT.value)

        self._power_off_button = Button(Pin.POWER_SWITCH.value)
        self._instructions_rotary_encoder = RotaryEncoder(Pin.INSTRUCTIONS_FORWARD_TRIGGER,
                                                          Pin.INSTRUCTIONS_BACKWARD_TRIGGER)

    def set_power_off_callback(self, callback):
        self._button_signaler.power_off.connect(callback)
        self._power_off_button.when_deactivated = self._button_signaler.power_off.emit

    def set_ingredients_callback(self, callback):
        self._button_signaler.ingredients.connect(callback)
        self._ingredientsButton.when_activated = self._button_signaler.ingredients.emit

    def set_instructions_callback(self, callback):
        self._button_signaler.instructions.connect(callback)
        self._instructionsButton.when_activated = self._button_signaler.instructions.emit

    def set_instructions_forward_callback(self, callback: Callable[[], None]):
        self._button_signaler.forward.connect(callback)
        self._instructions_rotary_encoder.forward_callback = self._button_signaler.forward.emit

    def set_instructions_backward_callback(self, callback: Callable[[], None]):
        self._button_signaler.backward.connect(callback)
        self._instructions_rotary_encoder.backward_callback = self._button_signaler.backward.emit

    def set_instructions_light(self, light_on: bool):
        self._instructionsLight.value = light_on

    def set_ingredients_light(self, light_on: bool):
        self._ingredientsLight.value = light_on

    def get_instructions_light(self) -> bool:
        return self._instructionsLight.value

    def get_ingredients_light(self) -> bool:
        return self._ingredientsLight.value
