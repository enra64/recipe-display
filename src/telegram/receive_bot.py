import logging
import os
from typing import Callable

import validators
from PyQt5.QtCore import pyqtSignal, QObject
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from src.util.load_recipe_signal import LoadRecipeSignaller


class ShutdownCallback(QObject):
    shutdown = pyqtSignal()


class ReceiveBot:

    def __init__(self, load_signaller: LoadRecipeSignaller, shutdown_callback: Callable):
        self._logger = logging.getLogger("receive_bot")
        self._token = os.getenv("TELEGRAM_BOT_TOKEN", "invalid")
        self._allowed_id = int(os.getenv("TELEGRAM_BOT_ALLOWED_ID", -1))
        self._updater = Updater(token=self._token)
        self._load_recipe_signaler = load_signaller

        self._shutdown_callback = ShutdownCallback()
        self._shutdown_callback.shutdown.connect(shutdown_callback)

        self._dispatcher = self._updater.dispatcher
        self._dispatcher.add_handler(CommandHandler("shutdown", self._shutdown))
        self._dispatcher.add_handler(MessageHandler(Filters.text, self._received_message))
        self._dispatcher.add_error_handler(lambda _, __, error: self._logger.warning(error))

    def _is_allowed(self, id):
        return id == self._allowed_id

    def start(self):
        self._updater.start_polling()

    def stop(self):
        self._updater.stop()

    def _extract_url(self, input: str) -> str:
        http_index = input.find("http")
        input = input[http_index:]
        input.split(" ")
        return input[0]

    def _received_message(self, bot, update):
        url = self._extract_url(update.message.text)
        if self._is_allowed(update.message.chat_id) and validators.url(url):
            self._logger.info("should load {}".format(url))
            self._load_recipe_signaler.load_recipe.emit(url)
            bot.send_message(chat_id=update.message.chat_id, text="Loading your recipe 👍")
        else:
            bot.send_message(chat_id=update.message.chat_id, text="I'm sorry, that didn't seem to be an url 😦")

    def _shutdown(self, bot, update):
        if self._is_allowed(update.message.chat_id):
            bot.send_message(chat_id=update.message.chat_id, text="Shutting down 😴")
            self._shutdown_callback.shutdown.emit()
