import logging
import os
import sys
from typing import Dict, Any, Callable

from PyQt5.QtWidgets import QApplication
from transitions import Machine

from src.chefkoch_api.chefkoch_recipes import get_recipe
from src.gpio.buttons import Buttons
from src.gpio.rpi_buttons import RpiButtons
from src.gui.main_window import MainWindow
from src.rewe_api import rewe_api
from src.speech.artificial_intelligence import ArtificialIntelligence
from src.speech.gtts_output import GttsSpeechOutput
from src.speech.input import SpeechInput
from src.speech.output import SpeechOutput
from src.speech.pyttsx_output import PyttsxSpeechOutput
from src.telegram.receive_bot import ReceiveBot
from src.util.config import config
from src.util.load_recipe_signal import LoadRecipeSignaller
from src.util.shutdown_handling import setup_interrupt_handling
from src.webserver.webserver import start_server


class RecipeMachine(Machine):
    def __init__(self, recipe_window: MainWindow, qtApp: QApplication, buttons: Buttons = None):
        self._logger = logging.getLogger(self.__class__.__name__)

        Machine.__init__(self, **self._get_state_machine_config())

        self._gui = recipe_window  # type: MainWindow

        if config.buttons.type == "keyboard":
            self._device_buttons = buttons  # type: Buttons
        elif config.buttons.type == "gpio":
            self._device_buttons = RpiButtons()  # type: Buttons
        else:
            self._logger.error("unknown buttons.type {}".format(config.buttons.type))
        self._configure_button_callbacks()

        def except_hook(cls, exception, traceback):
            logging.exception(exception)
            sys.__excepthook__(cls, exception, traceback)

        sys.excepthook = except_hook
        setup_interrupt_handling(self.power_off, lambda: self.is_state("shutdown", self.model))
        qtApp.aboutToQuit.connect(self.power_off)

        self._artificial_intelligence = None  # type: ArtificialIntelligence or None
        self._speech_input = None  # type: SpeechInput or None
        self._speech_output = None  # type: SpeechOutput or None
        self._server = None  # type: Callable or None
        self._telegram_bot = None  # type: ReceiveBot or None
        self._qtApp = qtApp
        self._load_recipe_signaller = LoadRecipeSignaller()

        self.initialize()

    @staticmethod
    def _get_state_machine_config() -> Dict[str, Any]:
        states = ["booted", "waiting", "displaying_ingredients", "displaying_instructions", "powering_off"]
        transitions = [
            {"trigger": "initialize", "source": "booted", "dest": "waiting"},
            {"trigger": "load_recipe", "source": ["waiting", "displaying_ingredients", "displaying_instructions"],
             "dest": "displaying_instructions",
             "before": "before_load_recipe"},
            {"trigger": "display_instructions", "source": ["displaying_ingredients", "displaying_instructions"],
             "dest": "displaying_instructions"},
            {"trigger": "display_ingredients", "source": ["displaying_ingredients", "displaying_instructions"],
             "dest": "displaying_ingredients"},
            {"trigger": "power_off", "source": "*", "dest": "powering_off"},
        ]
        return {"states": states, "transitions": transitions, "initial": "booted", "ignore_invalid_triggers": True}

    def _configure_button_callbacks(self):
        self._device_buttons.set_ingredients_callback(self.display_ingredients)
        self._device_buttons.set_instructions_callback(self.display_instructions)
        self._device_buttons.set_instructions_forward_callback(self._gui.forward_instructions)
        self._device_buttons.set_instructions_backward_callback(self._gui.backward_instructions)
        self._device_buttons.set_power_off_callback(self.power_off)

    def on_enter_waiting(self):
        if config.telegram_receive_bot.enable:
            self._telegram_bot = ReceiveBot(self._load_recipe_signaller, self.power_off)
            self._telegram_bot.start()

        if config.receive_server.enable:
            self._server = start_server(self._load_recipe_signaller)
            self._load_recipe_signaller.load_recipe.connect(self.load_recipe)

        if config.speech_output.tool == "google":
            self._speech_output = GttsSpeechOutput()
        elif config.speech_output.tool == "pyttsx":
            self._speech_output = PyttsxSpeechOutput()
        else:
            self._logger.error("unknown speech_output.tool {}".format(config.speech_output.tool))

        self._artificial_intelligence = ArtificialIntelligence(self._speech_output.say)

        if config.speech_recognition.enable:
            self._speech_input = SpeechInput(self._artificial_intelligence.on_audio_input)

    def before_load_recipe(self, link: str):
        if "chefkoch.de" in link:
            link = link.replace("https://www.chefkoch.de/rezepte/", "")
            recipe = get_recipe(int(link.split("/")[0]))
            self._gui.load_recipe("chefkoch", recipe)
            self._artificial_intelligence.recipe = recipe
        elif "rewe.de" in link:
            recipe = rewe_api.load_recipe(link)
            self._gui.load_recipe("chefkoch", recipe)
            self._artificial_intelligence.recipe = recipe
        else:
            self._gui.load_recipe("unknown", link)
            self._artificial_intelligence.recipe = None

    def on_enter_powering_off(self, _=None, __=None):
        if self._server is not None:
            self._server()

        if self._telegram_bot is not None:
            self._telegram_bot.stop()

        self._speech_output.stop()

        if self._speech_input:
            self._speech_input.stop()
        self._gui.stop()
        self._gui.close()
        if config.buttons.honor_shutdown_instruction:
            os.system('systemctl poweroff')
        else:
            self._logger.info("Skipping shutdown instruction, quitting app instead")
            self._qtApp.quit()

    def on_enter_displaying_ingredients(self, _=None, __=None):
        self._device_buttons.set_ingredients_light(True)
        self._gui.show_ingredients()

    def on_exit_displaying_ingredients(self, _=None, __=None):
        self._device_buttons.set_ingredients_light(False)

    def on_enter_displaying_instructions(self, _=None, __=None):
        self._device_buttons.set_instructions_light(True)
        self._gui.show_instructions()

    def on_exit_displaying_instructions(self, _=None, __=None):
        self._device_buttons.set_instructions_light(False)
