import argparse
import logging
import os

import yaml


def _obj_dic(d):
    top = type('new', (object,), d)
    seqs = tuple, list, set, frozenset
    for i, j in d.items():
        if isinstance(j, dict):
            setattr(top, i, _obj_dic(j))
        elif isinstance(j, seqs):
            setattr(top, i,
                    type(j)(_obj_dic(sj) if isinstance(sj, dict) else sj for sj in j))
        else:
            setattr(top, i, j)
    return top


class _Config:
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)

        parser = argparse.ArgumentParser(description='recipe-display')
        parser.add_argument('--configfile', help="Set the config file to load", required=False)
        self.args = parser.parse_args()

        if not self.args.configfile:
            self.args.configfile = "../assets/full.yaml"

        self._logger.info("Loading config from {}".format(self.args.configfile))
        with open(os.path.join(os.path.dirname(os.path.dirname(__file__)), self.args.configfile)) as yaml_config_file:
            self.config = yaml.safe_load(yaml_config_file)

        print(self.args)

    def __getattr__(self, name):
        try:
            return getattr(self.args, name)
        except AttributeError:
            return _obj_dic(self.config[name])


config = _Config()
