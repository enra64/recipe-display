from PyQt5.QtCore import QObject, pyqtSignal


class LoadRecipeSignaller(QObject):
    load_recipe = pyqtSignal(str)
