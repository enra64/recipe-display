# Call this function in your main after creating the QApplication
import signal
from typing import Any, Callable

from PyQt5 import QtCore


def setup_interrupt_handling(shutdown_handler: Callable[[Any, Any], None], should_continue: Callable):
    """Setup handling of KeyboardInterrupt (Ctrl-C) for PyQt."""
    signal.signal(signal.SIGINT, shutdown_handler)
    # Regularly run some (any) python code, so the signal handler gets a
    # chance to be executed:
    safe_timer(50, should_continue)


def safe_timer(timeout, func, *args, **kwargs):
    """
    Create a timer that is safe against garbage collection and overlapping
    calls. See: http://ralsina.me/weblog/posts/BB974.html
    """
    def timer_event():
        should_continue = True
        try:
            should_continue = func(*args, **kwargs)
        finally:
            if should_continue:
                QtCore.QTimer.singleShot(timeout, timer_event)
    QtCore.QTimer.singleShot(timeout, timer_event)