from typing import Dict, Union, List

from src.popo.ingredient import Ingredient
from src.popo.ingredient_group import IngredientGroup


class Recipe:
    def __init__(self, recipe_dict: Dict):
        self.title = recipe_dict["title"]  # type: str
        self.subtitle = recipe_dict["subtitle"]  # type: str
        self.instructions = \
            recipe_dict["instructions"].replace("\r\n", "<br>").replace("\n", "<br>")  # type: str

        self.ingredient_groups = []  # type: List[IngredientGroup]
        self._load_ingredient_groups(recipe_dict)

        self.sentences = [sentence + "." for sentence in self.instructions.split(".")]

        sentences_ignoring_abbreviation_points = []
        abbreviations = ["ca.", "min.", "sek.", "sec.", "bzw."]
        i = 0
        while i < len(self.sentences):
            sentence = self.sentences[i]
            if any(sentence.lower().endswith(abbreviation) for abbreviation in abbreviations):
                sentences_ignoring_abbreviation_points.append("{0} {1}".format(sentence, self.sentences[i + 1]))
                i += 2
            else:
                sentences_ignoring_abbreviation_points.append(sentence)
                i += 1

        self.paragraphs = self.instructions.split("<br><br>")
        self.highlighted_sentence_index = -1

    def get_currently_highlighted_sentence(self) -> Union[None, str]:
        if 0 <= self.highlighted_sentence_index < len(self.sentences):
            return self.sentences[self.highlighted_sentence_index]
        return None

    def get_current_paragraph(self):
        if self.highlighted_sentence_index < 0:
            return None

        for paragraph in self.paragraphs:
            if self.get_currently_highlighted_sentence() in paragraph:
                return paragraph

        return None

    def forward(self):
        self.highlighted_sentence_index = (self.highlighted_sentence_index + 1) % len(self.sentences)

    def backward(self):
        self.highlighted_sentence_index = \
            (self.highlighted_sentence_index + len(self.sentences) - 1) % len(self.sentences)

    def get_ingredients(self):
        ingredients = []
        for ingredient_group in self.ingredient_groups:
            ingredients.extend(ingredient_group.ingredients)
        return ingredients

    def _load_ingredient_groups(self, recipe_dict: Dict):
        for group_dict in recipe_dict["ingredientGroups"]:
            self.ingredient_groups.append(
                IngredientGroup(
                    group_dict["header"],
                    [Ingredient(ingredientDict) for ingredientDict in group_dict["ingredients"]]
                )
            )

    def get_ingredient_names(self):
        return "<br><br>".join([ingredient_group.__str__() for ingredient_group in self.ingredient_groups])
