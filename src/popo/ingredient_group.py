from typing import List

from src.popo.ingredient import Ingredient


class IngredientGroup:
    def __init__(self, name: str, ingredients: List[Ingredient]):
        self.name = name  # type: str
        self.ingredients = ingredients  # type: List[Ingredient]

    def __str__(self):
        ingredient_list = "<br>".join([ingredient.__str__() for ingredient in self.ingredients])
        if self.name == "":
            return ingredient_list
        else:
            return "<b>{}</b><br>{}".format(self.name, ingredient_list)
