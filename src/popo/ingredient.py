from typing import Dict


class Ingredient:
    def __init__(self, ingredient: Dict):
        self.name = ingredient["name"]  # type: str
        self.unit = ingredient["unit"]  # type: str
        self.amount = ingredient["amount"]  # type: str
        self.usageInfo = ingredient["usageInfo"]  # type: str

    def get_unit_name(self) -> str:
        unit = self.unit.lower()
        if unit == "g":
            return "gramm"
        if unit == "kg":
            return "kilogramm"
        if unit == "el":
            return "esslöffel"
        if unit == "tl":
            return "teelöffel"
        if unit == "ml":
            return "milliliter"
        if unit == "l":
            return "liter"
        return self.unit



    def __str__(self):
        def to_singular_quantifiers(string: str):
            if "(" in string:
                return string.split("(")[0]
            elif "/" in string:
                return string.split("/")[0]
            else:
                return string

        def to_plural(string: str):
            if "(" in string:
                return string.replace("(", "").replace(")", "")
            return string

        if len(self.unit) > 2:
            self.unit = " {}".format(self.unit)

        if not self.usageInfo.startswith(","):
            self.usageInfo = " {}".format(self.usageInfo)

        format_string = "{}{} {}{}"
        if self.amount == 0:
            self.unit = ""
            self.amount = ""
            format_string = "{}{}{}{}"
        elif self.amount == 1:
            self.unit = to_singular_quantifiers(self.unit)
            self.name = to_singular_quantifiers(self.name)
        else:
            self.unit = to_plural(self.unit)
            self.name = to_plural(self.name)

        return format_string.format(self.amount, self.unit, self.name, self.usageInfo)
