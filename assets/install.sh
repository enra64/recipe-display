#!/bin/bash

sudo apt-get update -y
sudo apt-get install python3-bs4 qt5-default pyqt5-dev python3-pyqt5-dev pyqt5-dev-tools python3-pyaudio flac
sudo python3 install -r requirements.txt