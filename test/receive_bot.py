from src.telegram.receive_bot import ReceiveBot
from src.util.load_recipe_signal import LoadRecipeSignaller


def shutdown_callback():
    print("shutting down...")


if __name__ == "__main__":
    load_recipe_signaller = LoadRecipeSignaller()
    load_recipe_signaller.load_recipe.connect(lambda url: print("load {}".format(url)))
    bot = ReceiveBot(load_recipe_signaller, shutdown_callback)
    bot.start()
