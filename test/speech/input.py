from time import sleep

from src.speech.input import SpeechInput


def test_full():
    si = SpeechInput(lambda recognized: print("recognized string: {}".format(recognized)))
    sleep(20)
    si.stop()


if __name__ == "__main__":
    test_full()
