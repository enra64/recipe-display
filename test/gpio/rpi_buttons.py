import os
import sys
from time import sleep

from src.gpio.rpi_buttons import RpiButtons


def test_output(btns: RpiButtons):
    btns.set_ingredients_light(True)
    btns.set_instructions_light(True)
    sleep(5)
    btns.set_ingredients_light(False)
    btns.set_instructions_light(False)
    sleep(.5)
    btns.set_ingredients_light(True)
    btns.set_instructions_light(True)


def test_input(btns: RpiButtons):
    btns.set_ingredients_callback(lambda: print("ingredients btn"))
    btns.set_instructions_callback(lambda: print("instructions btn"))
    btns.set_power_off_callback(lambda: print("power off button engaged"))
    btns.set_instructions_forward_callback(lambda: print("ingredients forward"))
    btns.set_instructions_backward_callback(lambda: print("ingredients backward"))


if __name__ == "__main__":
    param = sys.argv[1]
    btns = RpiButtons()

    if param.startswith("o"):
        print("output test")
        test_output(btns)
    elif param.startswith("i"):
        print("input test time starts")
        test_input(btns)
        sleep(20)
    elif param.startswith("p"):
        print("poweroff test")
        btns.set_power_off_callback(lambda: os.system('systemctl poweroff'))
        sleep(20)
    else:
        print("unknown argument {}".format(param))
    print("test end")
