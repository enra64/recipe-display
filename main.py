import logging
import subprocess

from src.gui.main_window import MainWindow

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='/tmp/recipeDisplay.log',
                    filemode='w')

import sys

from PyQt5 import QtWidgets
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QShortcut, QApplication

from src.gpio.mock_buttons import MockButtons
from src.util.config import config
from src.util.recipe_machine import RecipeMachine

recipeMachine = None
qtApp = None


def run(qt_app: QApplication) -> int:
    main_window = MainWindow()

    buttons = MockButtons()
    recipe_machine = RecipeMachine(main_window, qt_app, buttons)

    if config.buttons.type == "keyboard":
        QShortcut(QKeySequence("1"), main_window).activated.connect(buttons.instructions_callback)
        QShortcut(QKeySequence("2"), main_window).activated.connect(buttons.ingredients_callback)
        QShortcut(QKeySequence("3"), main_window).activated.connect(buttons.instructions_backward_callback)
        QShortcut(QKeySequence("4"), main_window).activated.connect(buttons.instructions_forward_callback)
        QShortcut(QKeySequence("q"), main_window).activated.connect(buttons.power_off_callback)

    try:
        auto_load_recipe_id = config.startup.autoload_recipe
        if auto_load_recipe_id:
            recipe_machine.load_recipe(auto_load_recipe_id)
    except KeyError:
        pass

    return qt_app.exec_()


def send_log():
    try:
        subprocess.call(["/home/pi/send_logs.sh"])
    except:
        pass


if __name__ == '__main__':
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter('%(name)-25s: %(levelname)-8s %(message)s'))
    logging.getLogger('').addHandler(console)

    qtApp = QtWidgets.QApplication(sys.argv)

    exit_code = run(qtApp)

    send_log()
    sys.exit(exit_code)
